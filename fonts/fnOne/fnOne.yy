{
    "id": "349ad3d2-8b80-444e-8aef-fb2b676d734c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnOne",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bc50950b-cbb8-42e0-a622-4336b43d01c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8b45e6dd-1802-4192-8ab2-1ed48a4a150e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "186d84a5-4f2b-49bd-ac52-1ee1c8d01430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 15,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ef577d25-715a-4f58-b65f-737851c23ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f91ccfd3-fa18-4682-b6b3-5492b9c3f8ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 242,
                "y": 52
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a5cfa824-ad1c-4aae-bad0-97007e75d948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 229,
                "y": 52
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d505da25-59df-4b43-af9a-94f327c1513a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 215,
                "y": 52
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d644974f-019f-49b4-837b-c743e72b570b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 210,
                "y": 52
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ccb52c5c-39ed-4934-960a-f032c500737d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 201,
                "y": 52
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9cf8a3d5-2208-4e73-8583-7ca973eb2d49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 192,
                "y": 52
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ac518925-0755-4b7f-8ad0-127e22da9625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 31,
                "y": 77
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3d0330e0-ff4b-4e04-84dc-d99bca05e29d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 179,
                "y": 52
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0cb4c54f-c174-4d53-b45a-18597ba03363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 160,
                "y": 52
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b2f8056d-246c-4d25-9069-018e723e0411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 151,
                "y": 52
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "38f701e6-f13e-45a2-9ee7-4e73a6721e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 144,
                "y": 52
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0282eb0a-e4f2-4cca-b54a-29907f39d91f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 132,
                "y": 52
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1e613ca4-db31-4f28-a4a5-5ed84c1e4892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 119,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cac33a67-e28c-4fd9-8d9a-249c754ac52d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 108,
                "y": 52
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "112fe245-4eae-47cf-bc6b-150b4f1b0d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 97,
                "y": 52
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d8f119da-232b-4dab-a375-c01cb9980c6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 86,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "add4ca25-c615-4dae-87b9-6f3823db2c3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 73,
                "y": 52
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8717dcd6-99a4-480a-b084-5bc811f9b448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 168,
                "y": 52
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "199d4058-328c-4a25-b1f8-b797127a8b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 42,
                "y": 77
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c34eb6b7-f7d4-4fc6-b294-19d5d4049f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 55,
                "y": 77
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "371d5111-ae2a-4c90-951b-c043c609ac9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 66,
                "y": 77
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "517617a0-95b2-42a9-8de2-b50c16da12d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 102
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "dba78cd6-e243-45bc-b686-a90aec0408ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 43,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "84249b69-e713-40dc-b739-a446d4d04e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 35,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "55958f46-4d67-4d3b-8696-75faa4416dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f9e65bec-bf7f-46ec-8190-f2c690c4e535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 102
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "38c2f2eb-7d39-4398-a047-586648245014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "55495124-911d-442d-b8a0-79160e5804f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 245,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "11c86f4e-c337-485f-ac60-b87a9004b456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 232,
                "y": 77
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "31ad379e-1f0e-4029-9d23-0a9b7b6f5297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 219,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6ed96949-02cb-4972-a787-46c38229821c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 207,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3d21ec7a-9ebc-4229-a75f-f97fef2628ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 195,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d8a4b7b3-f5b6-4fbd-bc5b-f0c24a167c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 182,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "46e4fa41-3f44-47be-abaa-0ab13d146322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7ddfb026-dce6-4a51-8df6-38bc1c45b405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 160,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "43bb4c13-6b07-4aa1-8c13-29d9eec058d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 147,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "74b5d5d9-7e68-4bb2-80a6-023138bacbd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 77
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8164fd6d-1e65-4c54-8cfc-956a915f3bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 123,
                "y": 77
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c8ff652f-526f-4041-ba67-cacc2a465e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 113,
                "y": 77
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "814e0c89-d8d5-4869-9f8b-950a2580589b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 101,
                "y": 77
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f3e0d4a8-88d7-4d45-b882-0f2b738c1a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "07e4c609-b30a-4b6b-a1ff-82bdd7ed2231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 77,
                "y": 77
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "756421a9-4a7f-4fa0-9d31-4377443ee966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 60,
                "y": 52
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "843736d8-562d-4565-9eea-f402ef847057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 47,
                "y": 52
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "23d5ec3d-1de6-4e76-a64f-f3b662d3392c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 35,
                "y": 52
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3592f8fa-dde8-4bdd-826e-91f5b03c91d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 11,
                "y": 27
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e2064913-3597-4b88-883b-555e40fb1c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c4fc9612-735d-4724-8973-8353f7d1a167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "84417460-32bd-45e3-b95f-3e689a60abb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "254794b8-24ff-43aa-9a23-1b2fdd039ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a76929d9-ce61-442b-bfac-49c72d3707e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "13584e0e-6782-446f-b972-83b99e263314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "321d23d9-5cc5-44ac-93cb-bda65848ab23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "61fc56a9-b4ba-44a5-b61a-6fafe967706e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "1eeb5db9-421c-4107-8512-39ea1365974b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ac0dbbe2-66e7-4553-af67-0d55afdd235b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4d11d9f2-363c-4eaa-b080-c033d57000ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d0bd8ddf-a203-480a-b177-1e7584bad9aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "316f12ac-9663-4435-8d3b-c55155860094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e96ec253-c167-4c67-8a61-d6d73b84e8c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8262c694-e560-4764-8e24-8df264e5ee5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9b7eaa44-8dc5-4af8-b14e-68ec85bd25c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3943773c-74fb-489a-a1bb-44dd6a082275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5347b1e4-2ef3-47a7-aae2-17d595f47d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2f28960a-b72e-4610-934b-6a89df3ed468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8df75252-a9a1-4b9e-b625-e046c9c1a109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a4d5a1cc-4037-4547-be77-ffce0cc53b59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "43a67d66-7054-40f9-886b-1d0816d3b926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 25,
                "y": 27
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "23217316-0382-4904-9d09-ef8336d3048c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 148,
                "y": 27
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5a9e2226-ff28-492c-8369-7bc0307cde0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 38,
                "y": 27
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1289a8e2-39c1-4ed7-97c8-a02e3a1e85ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 14,
                "y": 52
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d33a3bc1-dace-48d8-9827-13a39dffde98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "54656d0f-b49e-43ce-bd80-f618d59d6226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 27
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d3fe35be-ab43-42b2-9464-021a6a761bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 231,
                "y": 27
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cf6d418e-a100-4e36-a103-cc3458b61ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 220,
                "y": 27
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4760af9a-142b-457f-a6e0-09b3399ab1fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 207,
                "y": 27
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "73b68d1f-057b-44c5-9ec5-c235b0c96b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 195,
                "y": 27
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c5982dec-b96b-4dbe-9010-7d43f3ee2fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 183,
                "y": 27
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ef035677-e4d2-407b-8bff-29b48c9aad30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 171,
                "y": 27
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "320bfd2d-90f8-4b18-ad9d-27880e4287db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bef73221-3712-46b7-810e-69de8a2f775f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 159,
                "y": 27
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "10ec4743-e5f7-47f1-bb12-034a6cbdeeb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 137,
                "y": 27
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "350d724c-562b-4d74-bbbd-29c6d68b7f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 124,
                "y": 27
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "95a44696-d401-4884-9c9b-ec77f03af001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 111,
                "y": 27
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e60d690e-8b8a-4f12-beae-3bc5023359f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 98,
                "y": 27
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fead06ce-4be8-4552-addf-0e45eb3ffb42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 85,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d1527280-e918-4b03-8870-47f4eabde486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 74,
                "y": 27
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "16ef5c7a-bd7e-4146-b6a3-d0703ca056fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 64,
                "y": 27
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7f4e37d7-86bf-42fd-a8c4-6df5b987e51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 59,
                "y": 27
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dacf7559-3356-49ee-b8a6-5f5f0e87ea1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 49,
                "y": 27
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "158c7d50-4979-4254-bde5-d31bcb6f0f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 62,
                "y": 102
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e5fa1663-31bd-4c6e-ac85-73141f790338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 23,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 75,
                "y": 102
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 15,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}