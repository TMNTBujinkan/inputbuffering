{
    "id": "e82c323b-6cac-4743-ab5c-b57ad084ded0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "d5d8ce2d-e2db-4143-a5a5-29f7ad72403c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e82c323b-6cac-4743-ab5c-b57ad084ded0"
        },
        {
            "id": "02f132fb-8cf9-40b7-b245-dfd4e40f6680",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e82c323b-6cac-4743-ab5c-b57ad084ded0"
        },
        {
            "id": "35516d56-37cd-482c-be14-499277a596bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e82c323b-6cac-4743-ab5c-b57ad084ded0"
        },
        {
            "id": "293fae12-ec77-4a3f-a681-fd331c29e3d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e82c323b-6cac-4743-ab5c-b57ad084ded0"
        },
        {
            "id": "5c696400-8d28-49d1-abe8-7b34c8463ff4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e82c323b-6cac-4743-ab5c-b57ad084ded0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "42345a09-ac67-4466-b1c8-2ed61a83d42d",
    "visible": true
}