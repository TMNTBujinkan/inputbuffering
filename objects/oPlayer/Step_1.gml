/// @description Insert description here
// You can write your code in this editor

#region Get input from player

right	= keyboard_check(vk_right);
left	= keyboard_check(vk_left);
jump	= keyboard_check_pressed(vk_space);

#endregion

#region Add inputs to the list

if(jump)
{
	ds_list_add(inputList, "jump", current_time);	
}

#endregion

#region Loop through list and delete old inputs

for(var _i = 0; _i < ds_list_size(inputList); _i += 2)
{
	var _inputName	= inputList[| _i];
	var _inputTime	= inputList[| _i + 1];
	
	if(_inputName == "jump" && current_time > _inputTime + jumpInputLifeSpan)	
	{
		repeat(2)
		{
			ds_list_delete(inputList, _i);	
		}
	}
}

#endregion