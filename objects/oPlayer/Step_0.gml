/// @description Insert description here
// You can write your code in this editor

var _onGround	= place_meeting(x, y + 1, oWall);
var _input		= inputList[| 0];
var _move		= right - left;

#region Acceleration and friction

if(_move != 0)
{
	hspd = Approach(hspd, spd * _move, acceleration);
}
else
{
	hspd = Approach(hspd, 0, fric);
}

#endregion

#region Bunny jump, gravity and ledge forgiveness

// More info : http://kpulv.com/123/Platforming_Ledge_Forgiveness/

if(_onGround)
{
	lastTimeOnGround = current_time;
	
	if(_input == "jump")
	{
		yJumpStart	= y;
		vspd		= -sqrt(2 * grav * jumpHeight);
	}
}
else
{
	vspd = min(vspd + grav, maxJumpFallSpeed);
	
	if(current_time <= (lastTimeOnGround + ledgeForgiveness) && vspd >= 0 && _input == "jump")
	{
		yJumpStart	= y;
		vspd		= -sqrt(2 * grav * jumpHeight);
	}
	
	#region Debug
	
	if(vspd >= 0 && yJumpStart != 0)
	{
		show_debug_message("jump height : " + string(yJumpStart - y));
		yJumpStart = 0;
	}
	
	#endregion
}

#endregion

Collision();




