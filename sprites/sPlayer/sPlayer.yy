{
    "id": "42345a09-ac67-4466-b1c8-2ed61a83d42d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d22c4196-0a57-4af1-aeda-c261e869f359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42345a09-ac67-4466-b1c8-2ed61a83d42d",
            "compositeImage": {
                "id": "585c6a15-dcad-4122-b4da-896802f99cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d22c4196-0a57-4af1-aeda-c261e869f359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6a7f79-b75d-477c-b71e-7e25f45f0817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d22c4196-0a57-4af1-aeda-c261e869f359",
                    "LayerId": "985ce9d1-dc40-4cd6-9a4a-eb59b56c5c35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "985ce9d1-dc40-4cd6-9a4a-eb59b56c5c35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42345a09-ac67-4466-b1c8-2ed61a83d42d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}