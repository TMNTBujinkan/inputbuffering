{
    "id": "51480706-3c28-4c12-800b-3b407137f164",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "709cf2b6-dba7-43b9-a3e0-5d09635ea9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51480706-3c28-4c12-800b-3b407137f164",
            "compositeImage": {
                "id": "ae2cdfab-847c-4ed8-a5ca-57c044917043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "709cf2b6-dba7-43b9-a3e0-5d09635ea9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38831e4c-a2e7-4576-8de2-ade35711f599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "709cf2b6-dba7-43b9-a3e0-5d09635ea9b0",
                    "LayerId": "3c1bcffe-d144-408a-84a5-5376dbeff67a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3c1bcffe-d144-408a-84a5-5376dbeff67a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51480706-3c28-4c12-800b-3b407137f164",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}