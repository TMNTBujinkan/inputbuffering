/// @desc Approach()

/// @param start,end,step

var _start	= argument[0];
var _end	= argument[1];
var _step	= argument[2];

var _result;

if(_start < _end)
{
	_result = min(_start + _step, _end);
}
else
{
	_result = max(_start - _step, _end);
}

return(_result);