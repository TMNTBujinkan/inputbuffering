/// @desc Collision()

if(place_meeting(x + hspd, y, oWall))
{
	var _signHspd = sign(hspd);
	
	while(!place_meeting(x + _signHspd, y, oWall))
	{
		x += _signHspd;	
	}
	
	hspd = 0;
}
x += hspd;


if(place_meeting(x, y + vspd, oWall))
{
	var _signVspd = sign(vspd);
	
	while(!place_meeting(x, y + _signVspd, oWall))
	{
		y += _signVspd;	
	}
	
	vspd = 0;
}
y += vspd;